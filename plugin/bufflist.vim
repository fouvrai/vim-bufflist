
""""""""""""""""""""""""""""""""""""""""""""""""""
" BuffList
"

if exists("loaded_bufflist")
    finish
endif
let g:loaded_bufflist = 1

"
" Private
"

" Constants

let s:BuffName = "-BuffList-"

" Helpers

function! s:IsValidBuff(bufnr)
    if (!buflisted(a:bufnr))
        return 0
    endif

    return 1
endfunction

function! s:EnsureBuffListShowing()
    let l:bufnr = bufnr(s:BuffName)
    let l:winnr = bufwinnr(l:bufnr)

    if (l:winnr == -1)
        " create the BuffList window
        if (l:bufnr == -1)
            let l:cmd = "split " . s:BuffName
        else
            let l:cmd = "sbuffer " . l:bufnr
        endif

        exec "silent keepalt topleft " . l:cmd

        " ensure the BuffList is set up correctly
        setlocal buftype=nofile
        setlocal foldcolumn=0
        setlocal matchpairs=
        setlocal nobuflisted
        setlocal nomodifiable
        setlocal nonumber
        setlocal noswapfile
        setlocal statusline=%t
        setlocal updatetime=500
        setlocal winfixheight
        setlocal winfixwidth
        setlocal wrap

        noremap <silent> <buffer> l    :call search('[', 'w')<CR>
        noremap <silent> <buffer> h    :call search('[', 'bw')<CR>
        noremap <silent> <buffer> <CR> :call <SID>SelectFocusedBuff()<CR>

        if has("syntax")
            syn clear
            syn match BuffNormal               '\[[^\]]*\]'
            syn match BuffChanged              '\[[^\]]*\]+'
            syn match BuffVisibleNormal        '\[[^\]]*\]\*'
            syn match BuffVisibleChanged       '\[[^\]]*\]\*+'
            syn match BuffVisibleActive        '\[[^\]]*\]\*!'
            syn match BuffVisibleChangedActive '\[[^\]]*\]\*+!'

            hi def link BuffNormal               Comment
            hi def link BuffChanged              Ignore
            hi def link BuffVisibleNormal        Constant
            hi def link BuffVisibleActive        PreProc
            hi def link BuffVisibleChanged       Identifier
            hi def link BuffVisibleChangedActive Error
        endif
    endif

    return l:winnr
endfunction

function! s:FocusBuffList()
    let l:winnr = s:EnsureBuffListShowing()

    if (l:winnr != bufwinnr("%"))
        exec l:winnr . " wincmd w"
    endif
endfunction

function! s:BuildBuffList()
    let l:text = ""

    for l:bufnr in s:buffList
        let l:bufname = bufname(l:bufnr)

        if (!len(l:bufname))
            let l:bufname = "No Name"
        endif

        let l:text .= "[" . l:bufnr . ":" . fnamemodify(l:bufname, ":t") . "]"

        if (bufwinnr(l:bufnr) != -1)
            let l:text .= "*"
        endif

        if (getbufvar(l:bufnr, "&modified"))
            let l:text .= "+"
        endif

        if (l:bufnr == s:activeBuff)
            let l:text .= "!"
        endif
    endfor

    " only update if the text has changed
    if (l:text != s:buffText)
        " we need to update the buffer in a modifiable "block"
        setlocal modifiable
            " out with the old
            1,$d _

            " in with the new
            put! =l:text
            $d _
        setlocal nomodifiable

        let s:buffText = l:text
    endif
endfunction

function! s:ResizeBuffList()
    let l:width = winwidth("%")
    let l:col   = col("$")
    let l:lines = ceil(1.0 * l:col / l:width)

    exec "resize " . float2nr(l:lines)
endfunction

function! s:RefreshBuffList()
    call filter(s:buffList, 's:IsValidBuff(v:val)')

    if (1 < len(s:buffList))
        let s:activeBuff = bufnr("%")

        call s:FocusBuffList()
        call s:BuildBuffList()
        call s:ResizeBuffList()

        exec bufwinnr(s:activeBuff) . " wincmd w"
    else
        let l:buffListBufnr = bufnr(s:BuffName)

        if (l:buffListBufnr != -1)
            let l:buffListWinnr = bufwinnr(l:buffListBufnr)

            if (l:buffListWinnr != -1)
                exec "bd " . l:buffListBufnr
            endif
        endif
    endif
endfunction

function! s:SelectFocusedBuff()
    normal yi[
    let l:bufnr = str2nr(substitute(@", '\v^([0-9]*)', '\1', ''))
    let l:winnr = bufwinnr(l:bufnr)

    " if the Buff has a window, use that
    if (l:winnr != -1)
        exec l:winnr . " wincmd w"
    else
        wincmd p
        exec "b " . l:bufnr
    endif
endfunction

" Events

function! s:OnVimEnter()
    let s:buffList = range(1, bufnr("$"))
    call s:RefreshBuffList()
endfunction

function! s:NumericalSort(a, b)
    return (a:a - a:b)
endfunction

function! s:OnBufEnter(buffer)
    let l:bufnr      = str2nr(a:buffer)
    let l:inBuffList = (l:bufnr == bufnr(s:BuffName))

    " we do special work if we entered the BuffList
    if (l:inBuffList)
        " we don't do anything if we're the only window
        if (winbufnr(2) != -1)
            if (bufwinnr(s:BuffName) != winnr())
                b #
            else
                call s:RefreshBuffList()

                " move the cursor to the alt Buff
                let l:altnr = winbufnr(winnr("#"))
                call search('[' . l:altnr . ':', 'w')
            endif
        elseif (1 < len(s:buffList))
            let l:nextIndex = index(s:buffList, s:activeBuff) + 1

            if (len(s:buffList) <= l:nextIndex)
                let l:nextIndex = l:nextIndex - 2
            endif

            exec "b " . s:buffList[l:nextIndex]
            call s:RefreshBuffList()
        endif
    else
        " only set valid Buffs as active
        if (s:IsValidBuff(l:bufnr))
            if (index(s:buffList, l:bufnr) == -1)
                call add(s:buffList,  l:bufnr)
                call sort(s:buffList, "s:NumericalSort")
            endif

            let s:activeBuff         = l:bufnr
            let s:activeBuffModified = getbufvar(l:bufnr, "&modified")
        endif

        call s:RefreshBuffList()
    endif
endfunction

function! s:OnBufDelete(buffer)
    let l:bufnr = str2nr(a:buffer)

    " we don't care if the BuffList was deleted
    if (bufname(l:bufnr) != s:BuffName)
        let l:index = index(s:buffList, l:bufnr)

        " remove the buffer from our list of valid buffers
        if (l:index != -1)
            call remove(s:buffList, l:index)
            call s:RefreshBuffList()
        endif
    endif
endfunction

function! s:OnCursorHold()
    let l:bufnr = bufnr("%")

    " are we in the active Buff?
    if (l:bufnr == s:activeBuff)
        let l:modified = getbufvar(l:bufnr, "&modified")

        " has its modified state changed?
        if (l:modified != s:activeBuffModified)
            let s:activeBuffModified = l:modified
            call s:RefreshBuffList()
        endif
    endif
endfunction

let s:buffList = []
let s:buffText = ""

let s:activeBuff         = -1
let s:activeBuffModified = -1

"
" Mappings
"

noremap <C-TAB>   :bn<CR>
noremap <C-S-TAB> :bp<CR>

autocmd VimEnter    * call s:OnVimEnter()
autocmd BufEnter    * call s:OnBufEnter(expand("<abuf>"))
autocmd BufDelete   * call s:OnBufDelete(expand("<abuf>"))
autocmd CursorHold  * call s:OnCursorHold()
autocmd CursorHoldI * call s:OnCursorHold()

" vim:ft=vim:ff=unix

